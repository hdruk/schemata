# Column Name Schema

```txt
#/properties/dataElement/name#/definitions/dataElements/properties/name
```

The name of a column in a table.

> 255 Chars

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## name Type

merged type ([Column Name](dataset-definitions-dataelements-properties-column-name.md))

all of

*   [Untitled undefined type in HDR UK Dataset Schema](dataset-definitions-dataelements-properties-column-name-allof-0.md "check type definition")
