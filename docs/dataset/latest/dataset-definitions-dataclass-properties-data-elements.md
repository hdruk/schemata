# Data Elements Schema

```txt
#/properties/dataClass/elements#/definitions/dataClass/properties/elements
```

A list of data elements contained within a table in a dataset.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## elements Type

an array of merged types ([Details](dataset-definitions-dataclass-properties-data-elements-items.md))
