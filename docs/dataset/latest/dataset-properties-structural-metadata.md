# Structural Metadata Schema

```txt
#/properties/structuralMetadata#/properties/structuralMetadata
```

Descriptions of all tables and data elements that can be included in the dataset

> First phase includes only column level metadata, future versions will include value level attributes

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## structuralMetadata Type

an array of merged types ([Details](dataset-properties-structural-metadata-items.md))
