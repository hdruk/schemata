# Untitled undefined type in HDR UK Dataset Schema Schema

```txt
#/properties/dataClass/elements#/definitions/dataClass/properties/elements/items
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## items Type

merged type ([Details](dataset-definitions-dataclass-properties-data-elements-items.md))

all of

*   [Untitled undefined type in HDR UK Dataset Schema](dataset-definitions-dataclass-properties-data-elements-items-allof-0.md "check type definition")
