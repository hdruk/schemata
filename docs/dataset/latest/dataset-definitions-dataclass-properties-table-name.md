# Table Name Schema

```txt
#/properties/dataClass/name#/definitions/dataClass/properties/name
```

The name of a table in a dataset.

> Should be limited to 255 Characters, abstract text requires rewrite.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## name Type

merged type ([Table Name](dataset-definitions-dataclass-properties-table-name.md))

all of

*   [Untitled undefined type in HDR UK Dataset Schema](dataset-definitions-dataclass-properties-table-name-allof-0.md "check type definition")
