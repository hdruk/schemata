# Data Type Schema

```txt
#/properties/dataElement/dataType#/definitions/dataElement/properties/dataType
```

The data type of values in the column

> In future we could enumerate options for this, rather than just a string. 255 Chars

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## dataType Type

`string` ([Data Type](dataset-definitions-dataelement-properties-data-type.md))
