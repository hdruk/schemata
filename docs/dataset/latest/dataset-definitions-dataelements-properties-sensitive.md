# Sensitive Schema

```txt
#/properties/dataElement/sensitive#/definitions/dataElements/properties/sensitive
```

A True or False value, indicating if the field is sensitive or not

> We could clarify a definition of what is sensitive in the future.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## sensitive Type

`boolean` ([Sensitive](dataset-definitions-dataelements-properties-sensitive.md))
