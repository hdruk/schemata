# Untitled undefined type in HDR UK Dataset Schema Schema

```txt
#/properties/coverage/spatial#/definitions/coverage/properties/spatial/anyOf/1/items
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                        |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------------------------ |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [dataset.schema.json*](../../../schema/dataset/latest/dataset.schema.json "open original schema") |

## items Type

merged type ([Details](dataset-definitions-coverage-properties-geographic-coverage-anyof-1-items.md))

any of

*   [Untitled undefined type in HDR UK Dataset Schema](dataset-definitions-coverage-properties-geographic-coverage-anyof-1-items-anyof-0.md "check type definition")
